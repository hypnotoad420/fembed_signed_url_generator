// the purpose of this app is to act as a proxy when hitting fembed API, since fembed limits
// requests based on IP.
// One server/ip is sufficient to handle ~100 videos' worth of api traffic

let is_prod = process.env.NODE_ENV == "production"
let is_dev = !is_prod
let l = is_prod ? ()=>{} : console.log.bind(console)
let express = require('express')
let bodyParser = require('body-parser')
let _ = require('lodash')
let execSync = require('child_process').execSync

let app_port = 55123
let app = express()

app.use((req, res, next) => {
  res.removeHeader("X-Powered-By")
  next()
})
app.use(bodyParser.text({type:'application/json'}))
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
  extended: true
}))

app.post('/signed_urls', processQuery)
app.all('*', (req, res) => res.status(403).end())
l(`Starting app on port: ${app_port}`)
app.listen(app_port)

//===============================================================
async function processQuery(req, res, next) {
  try {
    let payload = req.body

    // l('payload:', payload)
    // l('file_id:', payload.file_id)

    let fembed_videos_manifest = generateFembedVideosManifest(payload.file_id)

    l(fembed_videos_manifest)

    res.send(fembed_videos_manifest)
  } catch(err) {
    l(err.stack)
    res.status(403).end()
  }
}

function generateFembedVideosManifest(file_id) {
  let result_string = execSync(`curl "https://www.fembed.com/api/source/${file_id}" -H "Accept: */*" -H "Referer: https://www.fembed.com/v/${file_id}" -H "Origin: https://www.fembed.com" -H "X-Requested-With: XMLHttpRequest" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36" -H "DNT: 1" -H "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" --data "r=^&d=www.fembed.com"`).toString()

  // l(result_string)
  let response_data = JSON.parse(result_string)

  if (response_data.success != true) {
    return null
  }

  // l(response_data)

  let fembed_videos_manifest = response_data.data

  // l(fembed_videos_manifest)

  let encodings = _.map(fembed_videos_manifest, (encoding) => {
    let redirector_url = encoding.file

    let signed_url = getSignedUrl(redirector_url)

    encoding.file = signed_url

    return encoding
  })

  return encodings
}

function getSignedUrl(redirector_url) {
  let result_string = execSync(`curl -I "${redirector_url}"`).toString()
  let lines = result_string.split("\n")

  let signed_url = null

  for (var i = 0; i < lines.length; ++i) {
    let line = lines[i]
    let parts = line.split(": ")
    let header_name = parts[0].toLowerCase()

    if ( header_name == 'location' ) {
      signed_url = parts[1].trim()
      break
    }
  }

  return signed_url
}